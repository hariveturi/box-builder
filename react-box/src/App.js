import React, { Component } from 'react';
import './App.css';
import MainBox from './Components/MainBox'

class App extends Component {
  // Initialize state - Numbers holds the current number of 
  state = {
    numbers: [],
    counter: 0
  };

// Deletes a box and then updates the state
  deleteBox = () => {
      let counter = this.state.counter;
      const numbers = [...this.state.numbers];
      counter-=1;
      numbers.pop();
      this.setState({numbers: numbers, counter: counter});
    }
   
  
  
  // Adds a box and then updates the state
  addBox = () => {
    let counter = this.state.counter;
    const numbers = [...this.state.numbers];
    counter+=1;
    numbers.push(counter);
    this.setState({numbers: numbers, counter: counter});
  }
  
  render() {
    let box = null;
    // Defining the box component
    box = (
      <div>
        {
          this.state.numbers.map((number, index) => <MainBox key={index} counterToken={number}/>)
          }
      </div>
    );
    if(this.state.counter>0){
    return (
      <div className="mpp">
        <h1>Box Builder</h1>
        <h3>Start Building</h3>
        <button className="addBtn" onClick={this.addBox}>Add box</button> <button className="rmvBtn" onClick={this.deleteBox}>Remove box</button>
        <div className="num_holder">
        {box}
        </div>
        <div className="sized_num" style={{fontSize: 30 + this.state.counter + 'px'}}>
        {this.state.counter}
        </div>
      </div>
    ); }
    else {
      return (
        <div className="mpp">
        <h1>Box Builder</h1>
        <h3>Start Building</h3>
        <button className="addBtn" onClick={this.addBox}>Add box</button> <button className="rmvBtn" onClick={this.deleteBox} disabled>Remove box</button>
        <div className="noBox">No Boxes</div>
      </div>
      );
    }
  }
}

export default App;
