Box-Builder (React.js & CSS)
------------
Components||
------------
=====================
App (Root component)||
=====================
The App component which is the parent component has another functional component called MainBox which produces a box with a number.
It has 3 states - 
number - Array (empty)
counter - number (0)
The app component returns the text “Box Builder, Start Building” and the buttons “Add Box” and “Remove Box”. (The Remove Box button will be disabled when counter is 0)
It also returns a component called MainBox
If counter is 0, the app component returns a component with text “No boxes”
An onClick event executes the addBox() function.
An onClick event executes the deleteBox() function.

========
MainBox||
========
This component takes a props from App that refers to the number array in the state and returns a span element with the number in the box.

-----------
Functions  |
-----------

--------
addBox()
--------
Copies the counter state variable and increments it by 1
Copies the number state array and adds the counter variable to the array
Updates state of App

------------
deleteBox()
------------
Copies the counter state variable and decrements it by 1
Copies the number state array and removes the last object using the pop method
Updates state of App

--------
Style
--------
As the MainBox component produces a span element, using CSS n-th child selector - odd, I styled the background color to be blue. Otherwise, it will be black.
Again using CSS n-th child selector - 5n, I styled the box to have a solid orange border
For the increasing and decreasing font size, the div element with class sized_num takes a style attribute that is set to a font-size of 30 and increments by 1 every time the state counter is updated.

